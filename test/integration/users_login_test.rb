require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
    #テスト前に実行する処理
  def setup
    #test/fixtures/users.ymlに書いた:tanakaをセット
    @user = users(:tanaka)
    
    #omniauthのテスト用オプション
    #/auth/twitterを叩くとtwitterに飛ばずに
    #/auth/twitter/callbackにリダイレクトされるようになる
    OmniAuth.config.test_mode = true
    
    #モックを作成。@tanakaに合わせて作っておく
    #request.env['omniauth.auth']に指定したのが入ると思うとよい。
     OmniAuth.config.mock_auth[:twitter] = OmniAuth::AuthHash.new({
      :provider => 'twitter',
      :uid => '123545',
      :info =>  { :name => "tanaka", 
                  :nickname => "tana_ka"
      }
    })
  end
  
  test "login and logout" do
    get login_path
    
    #ログインリンクがあるか確認
    assert_select "a[href=?]", "/auth/twitter"
    get "/auth/twitter"
    assert_redirected_to "/auth/twitter/callback"
    follow_redirect!
    
    #ログインに成功してユーザーページへリダイレクト
    # assert_redirected_to @user
    # follow_redirect!
    
    #ログイン後、ログアウトへのリンクがあるかチェック
    # assert_select "a[href=?]", logout_path
    # delete logout_path
    # assert_nil session[:user_id]
  end
end

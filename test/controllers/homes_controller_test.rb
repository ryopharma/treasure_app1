require 'test_helper'

class HomesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "Treasure -勉強投稿アプリ"
  end
  
  test "should get root" do
    get root_path
    assert_response :success
  end
  
  test "should get login" do
    get login_path
    assert_response :success
    assert_select "title", "#{@base_title}"
  end
  
  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "Treasureについて | #{@base_title}"
  end
  

  
  
  
end

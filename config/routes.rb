Rails.application.routes.draw do
  get '/index', to: 'homes#index'
  root 'homes#login'
  get '/about', to: 'homes#about'
  get '/auth/:provider/callback', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'
  get '/login', to: 'homes#login'
  resources :users
  resources :posts, only: [:new, :show, :create, :destroy], shallow: true do
  get :search, on: :collection
  resource :favorites, only: %i[create destroy]
  get :favorites, on: :collection
end

end

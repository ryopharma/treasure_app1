class FavoritesController < ApplicationController
  def create
    favorite = current_user.favorites.build(post_id: params[:post_id])
    favorite.save!
    # flash[:success] = "お気に入り登録！"
    redirect_to request.referrer || root_url
  end

  def destroy
    current_user.favorites.find_by(post_id: params[:post_id]).destroy!
    # flash[:success] = "お気に入り解除！"
    redirect_to request.referrer || root_url
  end
end


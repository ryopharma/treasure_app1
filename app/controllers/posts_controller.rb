class PostsController < ApplicationController
  before_action   :logged_in_user, only: [:new, :create, :destroy]
  before_action   :correct_user,   only: :destroy
  
  
  def new
    @post = current_user.posts.build
  end
  
  def show
    @post = Post.find_by(id: params[:id])
  end
  
  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "投稿できました"
      redirect_to current_user
    else
      @feed_items = []
      render 'new'
    end
  end
  
  def destroy
   if @post.destroy
    flash[:success] = "削除しました"
    redirect_to  current_user
   else
    flash[:danger] = "削除に失敗しました"
    redirect_to request.referrer || root_url
   end
  end
  
  def search
    #Viewのformで取得したパラメータをモデルに渡す
    @posts = Post.search(params[:search]).paginate(page: params[:page]).desc
  end
  
  private
  
    def post_params
      params.require(:post).permit(:title, :content, :picture)
    end
    
    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
  
  
end

class SessionsController < ApplicationController
    
    def create
      user = User.find_or_create_from_auth(auth_params)
      if user
        log_in user
        flash[:success] = 'ログインしました'
        redirect_to user
      else
        flash[:danger] = 'ログインに失敗しました'
        redirect_to login_path
      end
    end

  def destroy
    reset_session
    flash[:success] = 'ログアウトしました'
    redirect_to root_path
  end  
  
    private
    # ユーザー情報の入った
    def auth_params
      request.env["omniauth.auth"]
    end

end

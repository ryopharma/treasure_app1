class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :corrent_user, only: [:edit, :update]
  # before_action :admin_user, only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show
      @user = User.find_by(id: params[:id])
      @post = @user.posts.paginate(page: params[:page]).desc
      @posts = current_user.favorite_posts.includes(:user).paginate(page: params[:page]).desc
  end
  
  def edit
      @user = User.find(params[:id])
  end
  
  def update
      @user = User.find(params[:id])
        if @user.update_attributes(user_params)
          flash[:success] = "ユーザー情報を編集しました"
          redirect_to @user
        else
          render 'edit'
        end
  end
  
  def destroy
    User.find(params[:id]).destroy
    if current_user.admin? 
      flash[:success] = "削除しました！"
      redirect_to users_path
    else
      reset_session
      flash[:success] = "削除しました！"
      redirect_to root_path
    end
  end
  
  # beforeアクション
  
    # 正しいユーザーかどうか確認
  def corrent_user
    @user = User.find(params[:id])
    redirect_to login_path unless current_user?(@user)
  end
  
  
  private
    def user_params
        params.require(:user).permit(:name, :description, :picture)
    end
    
    # def admin_user
    #   redirect_to(root_path) unless current_user.admin?
    # end
  
  
end

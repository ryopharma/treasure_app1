class Post < ApplicationRecord
  belongs_to :user
  has_many :favorites, dependent: :destroy
  scope :desc, -> { order( created_at: :desc) }
  # default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 1000 }
  validates :title, presence: true, length: { maximum: 30 }
  validate :picture_size
  
  # postのお気に入り判定 → vies側で呼び出し
  def favorite_by?(user)
    favorites.where(user_id: user.id).exists?
  end
  
  def self.search(search)
    return Post.all.desc unless search
    Post.where(['title LIKE ?', "%#{search}%"])
  end

  
  private
  
  # アップロードされた画像のサイズをバリデーションする
   def picture_size
     if picture.size > 5.megabytes
       errors.add(:picture, "5MB以下でお願いします")
     end
   end
   
end

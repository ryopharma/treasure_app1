User.create!(name:  "Example User",
             image_url: "https://3.bp.blogspot.com/-LsO5L9qhYSM/U9y_Ur1kljI/AAAAAAAAjaw/Hq67ktTqOpM/s800/pasokon_kyoushitsu.png",
             description: "rarara")

99.times do |n|
  name  = Faker::Name.name
  image_url = "https://3.bp.blogspot.com/-LsO5L9qhYSM/U9y_Ur1kljI/AAAAAAAAjaw/Hq67ktTqOpM/s800/pasokon_kyoushitsu.png"
  description = "password"
  User.create!(name:  name,
               image_url: image_url,
               description: description)
end

users = User.order(:created_at).take(6)
50.times do
  title = Faker::Lorem.sentence(2)
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.posts.create!(title: title) }
  users.each { |user| user.posts.create!(content: content) }
end
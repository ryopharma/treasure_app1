class AddColumnUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :screen_name, :string
    add_column :users, :Twitter, :string
  end
end

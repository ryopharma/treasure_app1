class RemoveDetailsFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :screen_name, :string
    remove_column :users, :Twitter, :string
  end
end

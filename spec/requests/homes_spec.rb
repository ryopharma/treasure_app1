require 'rails_helper'

RSpec.describe "Homes", type: :request do
  describe "home_page" do
    before do
     get root_path
    end
    context 'Home_page is good' do  
      # Homeページからroot,about_pathに問題なくいけるか
      it "response the root_path" do
        expect(response).to have_http_status(200)
      end
      it "have the root_correct_title" do
        expect(response.body).to include 'Treasure -勉強投稿アプリ'
        expect(response.body).to_not include '| Treasure -勉強投稿アプリ'
      end
    end
  
    context 'about_page is good' do
      # Homeページのそれぞれの画面のタイトルが正しいかどうか
      it "response the about_path" do
        get about_path
        expect(response).to have_http_status(200)
      end
      it "have the about_correct_title" do
        get about_path
        expect(response.body).to include 'Treasureについて | Treasure -勉強投稿アプリ'
      end
    end
  end
end
